import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NotificationsService } from '../../services/notifications/notifications.service';
import { AlertController } from '@ionic/angular';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'app-notify',
  templateUrl: './notify.component.html',
  styleUrls: ['./notify.component.scss'],
})
export class NotifyComponent implements OnInit {

  intervals: any;
  user_id: any;
  response: any;
  private clickSound = new Audio('assets/audio/ding.mp3');
  constructor(
    public App_component: AppComponent,
    public alertCtrl: AlertController,
    public storage: Storage,
    public notificationAPI: NotificationsService,
  ) {

    this.storage.get('user_id').then((response) => {
      if (response) {
        this.user_id = response;
        this.LoopFunction();
      }
    });

  }//contructor ends here 

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      // cssClass: 'my-custom-class',
      header: 'Alert',
      subHeader: 'Subtitle',
      message: 'This is an alert message.',
      buttons: ['OK']
    });

    await alert.present();
  }

  ngOnInit() { }

  //this is sued to call a function after every x seconds to check if there is any unread notification
  LoopFunction() {
    this.intervals = setInterval(() => {
      this.GetNotifications();
    }, 20000);
  }//end of LoopFunction

  GetNotifications() {
    this.notificationAPI
      .get_notifications_unread(this.user_id) //api service method
      .subscribe((response) => {
        this.response = response;
        if (this.response.code == 1) {
          this.App_component.unread = this.response.count;
          if (this.response.counter > 0) {
            this.clickSound.play();
          }
          // this.presentAlert();
        }
        else if (this.response.code == 0) {
          this.App_component.unread = 0;
        }
      });
  }//end of GetNotifications


}
