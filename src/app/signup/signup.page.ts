import { NavController, NavParams } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { RegisterService } from '../services/register/register.service';
import { ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {


  role_id: any = '2';
  response: any;

  constructor(
    public router: Router,
    public fb: FormBuilder,
    public navCtrl: NavController,
    public Api: RegisterService,
    private toastCtrl: ToastController,
    public storage: Storage,
  ) {

    //this is the code in terms of auth guards are not working
    // this.storage.get('user_id').then((response) => {
    //   if (response) {
    //     //alert('user_id ' + response);
    //     this.router.navigate(['/recent-broadcasts']);
    //   }
    //   else {
    //     //alert('user_id not found');
    //     this.router.navigate(['/signup']);
    //   }
    // });
    //this is the code in terms of auth guards are not working ends here

  }
  ngOnInit() { }

  async presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    (await toast).present();
  }

  public registerForm = this.fb.group({
    name: ["", Validators.compose([Validators.required, Validators.minLength(3)])],
    email: ["", Validators.compose([Validators.required])],
    password: ["", Validators.compose([Validators.required, Validators.minLength(8)])],
    confirmpassword: ["", Validators.compose([Validators.required, Validators.minLength(8)])]
  });

  async register_user() {

    this.GetRoleID();

    var formdata = {
      name: this.f.name.value,
      email: this.f.email.value,
      password: this.f.password.value,
      role_id: this.role_id,
    };

    await this.Api
      .register_user(formdata) //api service method
      .subscribe((response) => {
        this.response = response;
        console.log(this.response);
        if (this.response.code == 0) {
          //this.presentToast(this.response.message);
          alert('Error Cannot Register');
        }
        else if (this.response.code == 1) {
          // console.log('user _id is ', this.response.user);
          this.storage.set('user_id', this.response.user.id);
          // this.presentToast(this.response.message);
          this.router.navigate(['recent-broadcasts']);
        }
        else if (this.response.code == -1) {
          alert(this.response.message);
        }
      },
        (err) => {
          this.presentToast(err.message);
        }
      );
  }//end of register_user


  get f() { return this.registerForm.controls; }

  //getting role id of user 
  async GetRoleID() {

    this.Api.get_role_id().subscribe(response => {
      if (response) {

        this.role_id = response;
        console.log('role id ', this.role_id);

      }
    }, error => {
      console.log("Error: ");
      console.log(error);
    });
  }

  gotoLogin() {
    console.log('gotoLogin');
    // this.navCtrl.push(LoginPage);

  }

}
