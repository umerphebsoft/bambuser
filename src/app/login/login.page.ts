import { NavController, AlertController, NavParams, ToastController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../services/login/login.service';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {


  response: any;
  constructor(
    public alertController: AlertController,
    public router: Router,
    public fb: FormBuilder,
    public navCtrl: NavController,
    public Api: LoginService,
    private toastCtrl: ToastController,
    public storage: Storage,
  ) {

    //this is the code in terms of auth guards are not working
    // this.storage.get('user_id').then((response) => {
    //   if (response) {
    //     // alert('user_id ' + response);
    //     this.router.navigate(['/recent-broadcasts']);
    //   }
    //   else {
    //     // alert('user_id not found');
    //     this.router.navigate(['/login']);
    //   }
    // });
    //this is the code in terms of auth guards are not working ends here

  }

  ngOnInit() { }

  ionViewWillEnter() {
    this.f.email.setValue('');
    this.f.password.setValue('');


  }

  async presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    (await toast).present();
  }

  public loginForm = this.fb.group({
    email: ["", Validators.compose([Validators.required])],
    password: ["", Validators.compose([Validators.required, Validators.minLength(8)])],
  });

  async login_user() {

    var formdata = {
      email: this.f.email.value,
      password: this.f.password.value,
    };

    await this.Api
      .login_user(formdata) //api service method
      .subscribe((response) => {
        this.response = response;
        console.log(this.response);
        if (this.response.code == 0) {
          this.presentToast(this.response.message);
          alert(this.response.message);

        }
        else if (this.response.code == 1) {
          this.storage.set('user_id', this.response.user.id);
          this.presentToast(this.response.message);
          this.router.navigate(['/recent-broadcasts']);
        }
      },
        (err) => {
          this.presentToast(err.message);
        }
      );
  }//end of register_user


  get f() { return this.loginForm.controls; }

  gotoRegister() {
    console.log('gotoRegister');
    // this.navCtrl.push(RegisterPage);
  }



}
