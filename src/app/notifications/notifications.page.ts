import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NotificationsService } from '../services/notifications/notifications.service';
import { Router } from '@angular/router';
import { ToastController, NavController, AlertController } from '@ionic/angular';
import { FriendService } from '../services/friends/friend.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {

  user_id: any;
  response: any;
  notifications: any;
  items = [];
  mydone = true;

  constructor(
    public frndAPI: FriendService,
    public toast: ToastController,
    public router: Router,
    public notificationAPI: NotificationsService,
    public storage: Storage,
  ) {

  }

  //toast controller function
  async showToastWithOptions() {
    const toast = await this.toast.create({
      cssClass: 'toastTag',
      color: "primary",
      position: 'middle',
      message: "Swipe Left to Action",
      duration: 2000,
    });
    toast.present();
  }

  Add_friend(from_id, id) {
    var formdata = {
      "user_id": this.user_id,
      "from_id": from_id
    };

    this.frndAPI.add_friend(formdata) //api service method
      .subscribe((response) => {
        this.response = response;
        if (this.response.code == 0) {
          //this.presentToast(this.response.message);
          alert('Error Cannot Add to friend list');
        }
        else if (this.response.code == 1) {
          var ele = 'btn_acpt_' + id;
          var btn = document.getElementById(ele);
          btn.remove();
          this.Delete_request(id);
        }
      },
        (err) => {
          alert('Error : ' + err.message);
        }
      );
  }//end of Add_friend

  Delete_request(id) {
    this.notificationAPI.delete_notification(id) //api service method
      .subscribe((response) => {
        this.response = response;
        if (this.response.code == 0) {
          //this.presentToast(this.response.message);
          alert('Error Cannot Cancel friend list');
        }
        else if (this.response.code == 1) {
          var ele = 'div_' + id;
          var btn = document.getElementById(ele);
          btn.remove();
        }
      },
        (err) => {
          alert('Error : ' + err.message);
        }
      );

  }//end of Delete_request


  Mark_Read(id) {

    this.notificationAPI.mark_read_notify(id).subscribe(response => {
      this.response = response;
      if (this.response.code == 0) {
        alert('Error: Cannot mark as read');
      }
      else if (this.response.code == 1) {
        var ele = 'btn_' + id;
        var btn = document.getElementById(ele);
        btn.remove();
      }
    }, error => {
      alert('Error: cannot mark as read' + error.message);
    });
  }//end of mark read

  ngOnInit() {
  }

  playBroadcast(broadcast) {
    // if (broadcast.type === 'live' && window['bambuser'] && window['bambuser']['player']) {
    // alert('resource uri ' + broadcast);
    this.router.navigate(['native-player', { resourceUri: broadcast, autoplay: true }]);
    // } else {
    //   this.router.navigate(['native-player', { resourceUri: broadcast, autoplay: true }]);
    // }
  }//playBroadcast

  ionViewWillEnter() {

    // this.showToastWithOptions();
    this.storage.get('user_id').then((response) => {
      if (response) {
        this.user_id = response;
        this.GetNotifications();
      }
    });
  }

  loadData(event) {
    if (this.items.length != this.notifications.length) {
      var length = this.items.length;
      console.log('length is ', length);
      if ((this.notifications.length - length) >= 5) {
        for (var i = length; i <= (length + 5); i++) {
          this.items.push(this.notifications[i]);
        }//end of for
      }
      else {
        for (var i = length; i <= this.notifications.length; i++) {
          this.items.push(this.notifications[i]);
        }//end of for
      }
    }
    else {
      this.mydone = false;
    }

  }//end of loadData

  GetNotifications() {
    var counter = 0;
    this.notificationAPI.get_notifications(this.user_id).subscribe(response => {
      this.response = response;
      if (this.response.code == 1) {
        this.notifications = this.response.notifications;
        this.notifications.forEach(element => {
          this.items.push(element);
          counter++;
          if (counter == 5) {
            return false;
          }
        });//end of foreach loop to put 10 items in items 
      }
      else if (this.response.code == 0) {
        this.notifications = [];
        alert('Error: cannot get notifications');
      }
    }, error => {
      this.notifications = [];
      alert('Error: cannot get notifications ' + error.message);
    });


  }//end of GetNotifications

  ionViewWillLeave() {
    this.items = [];
  }//ionViewWillLeave

}
