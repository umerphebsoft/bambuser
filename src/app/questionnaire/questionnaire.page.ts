import { Component, OnInit } from '@angular/core';
import { QuestionsService } from '../services/questionnaire/questions.service';
import { Storage } from '@ionic/storage';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
@Component({
  selector: 'app-questionnaire',
  templateUrl: './questionnaire.page.html',
  styleUrls: ['./questionnaire.page.scss'],
})
export class QuestionnairePage implements OnInit {

  broadcast_id: any = '';
  input = [];
  // broadcast_id: any = 'asdasdasd';
  main_questions: any;
  questions: any = [];
  response: any;
  ques_true = false;
  answers: any = [];

  constructor(
    public router: Router,
    public loadingController: LoadingController,
    public storage: Storage,
    private questionAPI: QuestionsService,
  ) {
    this.storage.get('broadcast_id').then((response) => {
      if (response) {
        this.broadcast_id = response;
      }
    });

    this.GetQuestionCategory();
  }

  ngOnInit() {
  }


  Answer(question_id, answer_id) {

    this.answers.forEach((value, index) => {
      if (value.question_id == question_id) {
        this.answers.splice(index, 1);
      }
    });
    var obj = { "question_id": question_id, "answer": answer_id.detail.value };
    this.answers.push(obj);
    console.log('question id pushed', question_id);

    if (this.answers.length == this.questions.length) {
      this.SubmitAnswers();
    }
  }

  selectedQuestion(id) {
    this.GetSubQuestions(id);
  }

  SubmitAnswers() {

    var mythis = this;
    //getting users input fields values adn saving them in obj
    this.answers.forEach(element => {
      this.input.forEach((value, index) => {
        if (element.question_id == index) {
          mythis.answers.splice(index, 1);
          console.log('question id matched ', index);
        }
      });
    });

    this.input.forEach((value, index) => {
      var obj = { "question_id": index, "answer": value };
      console.log('question id pushed', index);
      mythis.answers.push(obj);
    });

    if (this.answers.length == this.questions.length) {

      var data = {
        broadcast_id: this.broadcast_id,
        answers: this.answers
      };
      this.questionAPI.submit_answers(data).subscribe(response => {
        this.response = response;
        if (this.response.code == 1) {
          this.router.navigate(['thanks']);
        }
        else if (this.response.code == 0) {
          alert('Something went wrong. Cannot save your answers. Please Submit Again');
        }

      }, error => {
        console.log("Error: ");
        alert('Error: ' + error.message);
        console.log(error);
      });

    }//end of if user has submitted all question answers
    else {
      alert('Please Answer All Questions.');
    }

  }//end of SubmitAnswers



  async GetSubQuestions(id) {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 8000
    });
    loading.present();

    this.questionAPI.get_sub_questions(id).subscribe(response => {
      this.response = response;
      if (this.response.code == 0) {
        loading.dismiss();
        alert('Error: cannot get questions');
      }
      else if (this.response.code == 1) {
        loading.dismiss();
        this.questions = this.response.questions;
        this.ques_true = true;
        console.log('questions ', this.questions);
      }
      else {
        loading.dismiss();
        this.questions = [];
      }
    }, error => {
      loading.dismiss();
      console.log("Error: ");
      alert('Error: ' + error);
      console.log(error);
    });

  }//end of get sub questions

  GetQuestionCategory() {
    this.questionAPI.get_main_questions().subscribe(response => {
      this.response = response;
      if (this.response.code == 0) {
        alert('Error: cannot get questions');
      }
      else if (this.response.code == 1) {
        this.main_questions = this.response.questions;
        console.log('questions ', this.main_questions);
      }
      else {
        this.main_questions = [];
      }
    }, error => {
      console.log("Error: ");
      alert('Error: ' + error);
      console.log(error);
    });

  }//end of GetQuestionCategory

}
