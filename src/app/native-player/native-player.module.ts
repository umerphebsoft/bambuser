import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NativePlayerPageRoutingModule } from './native-player-routing.module';

import { NativePlayerPage } from './native-player.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NativePlayerPageRoutingModule
  ],
  declarations: [NativePlayerPage]
})
export class NativePlayerPageModule {}
