import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NativePlayerPage } from './native-player.page';

const routes: Routes = [
  {
    path: '',
    component: NativePlayerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NativePlayerPageRoutingModule {}
