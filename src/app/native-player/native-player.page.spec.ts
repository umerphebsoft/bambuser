import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NativePlayerPage } from './native-player.page';

describe('NativePlayerPage', () => {
  let component: NativePlayerPage;
  let fixture: ComponentFixture<NativePlayerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NativePlayerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NativePlayerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
