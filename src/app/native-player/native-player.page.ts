import { Component, OnInit, ElementRef, NgZone, ViewChild } from '@angular/core';

import { NavController, NavParams, Platform } from '@ionic/angular';

import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { SettingService } from '../services/setting/setting.service';
@Component({
  selector: 'app-native-player',
  templateUrl: './native-player.page.html',
  styleUrls: ['./native-player.page.scss'],
})
export class NativePlayerPage implements OnInit {

  @ViewChild('player') playerEl: ElementRef;
  playerLog = [];
  showCloseButton = true;

  resourceUri = '';

  public navParams = new NavParams

  constructor(
    public route: ActivatedRoute,
    private element: ElementRef,
    public navCtrl: NavController,

    private zone: NgZone) {
    // console.log('asdasdads');



    this.resourceUri = this.route.snapshot.paramMap.get('resourceUri');
    // alert('resource uri in native player' + this.resourceUri);

  }

  ngOnInit() { }

  ionViewDidEnter() {
    console.log('starting javascript player');

    // alert('starting javascript player');

    // BambuserPlayer is loaded to window in index.html
    const BambuserPlayer: any = window['BambuserPlayer'];
    // https://bambuser.com/docs/key-concepts/resource-uri/
    // The resourceUri is used to fetch the broadcast media
    //
    // Either use a broadcast provided by another page in the application

    if (!this.resourceUri) {
      // ...or fall back to using a static resourceUri, for demo purposes
      this.resourceUri = 'https://cdn.bambuser.net/broadcasts/0b9860dd-359a-67c4-51d9-d87402770319?da_signature_method=HMAC-SHA256&da_id=9e1b1e83-657d-7c83-b8e7-0b782ac9543a&da_timestamp=1482921565&da_static=1&da_ttl=0&da_signature=cacf92c6737bb60beb1ee1320ad85c0ae48b91f9c1fbcb3032f54d5cfedc7afe';

      // normally you would get the resourceUri from the GET /broadcasts API
      // either by directly accessing it from your mobile app, or with your
      // custom backend as mediator.
      // https://bambuser.com/docs/api/get-broadcast-metadata/
    }

    // https://bambuser.com/docs/playback/web-player/#javascript-api
    const player = BambuserPlayer.create(this.playerEl.nativeElement, this.resourceUri);
    player.controls = true;
    player.autoplay = true;
    player.scaleMode = true;

    const log = str => {
      // Ensure template is re-rendered even though caller might be an
      // event listener on an emitter outside of Angular's zone.
      // https://angular.io/docs/ts/latest/api/core/index/NgZone-class.html
      this.zone.run(() => {
        this.playerLog.unshift(`${player.currentTime} ${player.duration} ${str}`);
      });
    }

    // Make player available in console, for debugging purposes
    console.log('The player object is now assigned to window.player to enable manual debugging of the player API. Try player.pause(), player.play(), reading from and assigning to player.currentTime etc...', player);
    window['player'] = player;

    // Log all player events as they occur, for debugging purposes
    [
      'canplay',
      'durationchange',
      'ended',
      'error',
      'loadedmetadata',
      'pause',
      'play',
      'playing',
      'progress',
      'seeked',
      'seeking',
      'timeupdate',
      'volumechange',
      'waiting'
    ].map(eventName => player.addEventListener(eventName, e => log(eventName)));

    if (this.navParams.get('autoplay')) {
      // Does not work in all circumstances - see notes at
      // https://bambuser.com/docs/playback/web-player/#javascript-api
      player.play();
    }

    if (this.navParams.get('showCloseButton')) {
      this.showCloseButton = true;
    }
  }

  closePlayer() {
    // Relevant only if player is opened as a modal
    this.navCtrl.pop();
  }

  ionViewWillLeave() {
    // Remove player from DOM.
    //
    // By design, Ionic tabs controller keeps the page alive in the background
    // when navigating away. Leaving a player in the background might be
    // resource-consuming and otherwise unexpected though...
    //
    // If retaining the player state is desired when navigating back and forth,
    // consider replacing the below assignment with player.pause() / player.play()

    console.log('closing javascript player');
    // alert('closing javascript player');
    this.playerEl.nativeElement.innerHTML = '';
    this.playerLog = [];
  }


































  // public navParams = new NavParams;

  // resourceUri = '';

  // APPLICATION_ID: string = '';
  // response: any;

  // player: any;
  // playerLog = [];
  // showCloseButton = true;

  // constructor(
  //   public api: SettingService,
  //   public navCtrl: NavController,
  //   public route: ActivatedRoute,
  //   public platform: Platform,
  //   private zone: NgZone) {

  //   this.resourceUri = this.route.snapshot.paramMap.get('resourceUri');

  //   this.GetApplicationID();

  // }//end of constructor

  // // https://bambuser.com/docs/key-concepts/resource-uri/




  // ngOnInit() {
  //   // ...
  // }

  // GetApplicationID() {
  //   console.log('read only api');
  //   this.api.get_app_id().subscribe(response => {
  //     if (response) {

  //       this.response = response;
  //       this.APPLICATION_ID = this.response.app_id;
  //     }
  //     else {
  //       this.APPLICATION_ID = '';
  //     }
  //     alert('app id ' + this.APPLICATION_ID);
  //     console.log('application id ', this.APPLICATION_ID);

  //     this.platform.ready().then(() => {
  //       // Using array syntax workaround, since types are not declared.
  //       if (window['bambuser'] && window['bambuser']['player']) {
  //         this.player = window['bambuser']['player'];
  //         this.player.setApplicationId(this.APPLICATION_ID);
  //         alert('native player assigned');
  //       } else {
  //         console.log('Cordova plugin not installed or running in a web browser');
  //         alert('Cordova plugin not installed or running in a web browser');
  //         // Cordova plugin not installed or running in a web browser
  //       }
  //     });

  //   }, error => {
  //     console.log("Error: ");
  //     console.log(error);
  //   });
  // }


  // ionViewDidEnter() {
  //   console.log('starting native player');

  //   if (this.APPLICATION_ID.endsWith('NGEME')) {
  //     new Promise(resolve => setTimeout(resolve, 500)).then(() => {
  //       // Let page animations to finish before using alert()
  //       alert('Warning: APPLICATION_ID is not set. Get your application id at https://dashboard.bambuser.com/developer and update pages/player-native/player-native.ts, then rebuild the app.');
  //     });
  //     return;
  //   }

  //   if (!this.player) {
  //     new Promise(resolve => setTimeout(resolve, 500)).then(() => {
  //       // Let page animations to finish before using alert()
  //       alert('Native player is not ready yet');
  //     });
  //     return;
  //   }

  //   console.log('Displaying player behind webview');
  //   alert('Displaying player behind webview');

  //   // Engage our Ionic CSS background overrides that ensure native player behind webview is visible.
  //   document.getElementsByTagName('body')[0].classList.add("show-native-player");

  //   this.player.showPlayerBehindWebView();

  //   // https://bambuser.com/docs/key-concepts/resource-uri/
  //   // The resourceUri is used to fetch the broadcast media
  //   //
  //   // Either use a broadcast provided by another page in the application

  //   alert('resource uri : ' + this.resourceUri);
  //   if (!this.resourceUri) {
  //     // ...or fall back to using a static resourceUri, for demo purposes
  //     this.resourceUri = 'https://cdn.bambuser.net/broadcasts/0b9860dd-359a-67c4-51d9-d87402770319?da_signature_method=HMAC-SHA256&da_id=9e1b1e83-657d-7c83-b8e7-0b782ac9543a&da_timestamp=1482921565&da_static=1&da_ttl=0&da_signature=cacf92c6737bb60beb1ee1320ad85c0ae48b91f9c1fbcb3032f54d5cfedc7afe';

  //     // normally you would get the resourceUri from the GET /broadcasts API
  //     // either by directly accessing it from your mobile app, or with your
  //     // custom backend as mediator.
  //     // https://bambuser.com/docs/api/get-broadcast-metadata/
  //   }

  //   const log = str => {
  //     // Ensure template is re-rendered even though caller might be an
  //     // event listener on an emitter outside of Angular's zone.
  //     // https://angular.io/docs/ts/latest/api/core/index/NgZone-class.html
  //     this.zone.run(() => {
  //       this.playerLog.unshift(`${moment().format('hh:mm:ss')} ${str}`);
  //     });
  //   }

  //   // Log all player events as they occur, for debugging purposes
  //   [
  //     'broadcastLoaded',
  //     'stateChange',
  //   ].map(eventName => this.player.addEventListener(eventName, e => log(eventName + ': ' + JSON.stringify(e))));

  //   if (this.navParams.get('showCloseButton')) {
  //     this.showCloseButton = true;
  //   }

  //   this.player.loadBroadcast(this.resourceUri);
  // }

  // closePlayer() {
  //   // Relevant only if player is opened as a modal
  //   this.navCtrl.pop();
  // }

  // ionViewWillLeave() {
  //   alert('closing native player');
  //   console.log('closing native player');

  //   // Disengage our Ionic CSS background overrides, to ensure the rest of the app looks ok.
  //   document.getElementsByTagName('body')[0].classList.remove("show-native-player");

  //   if (this.player) {
  //     this.player.hidePlayer();
  //     this.player.close();
  //   }
  // }

}
