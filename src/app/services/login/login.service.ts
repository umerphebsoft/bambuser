import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Configs } from '../../config';

const config = new Configs;
const apiUrl = config.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(public https: HttpClient) { }

  login_user(data) {
    const httpHeader = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    const headers = {
      headers: httpHeader
    };
    const url = `${apiUrl}/login_user`; //route of laravel  
    return this.https.post(url, data, headers);
  }

}
