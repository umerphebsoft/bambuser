import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Configs } from '../../config';

const config = new Configs;
const apiUrl = config.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class QuestionsService {

  constructor(public https: HttpClient) { }

  get_main_questions() { return this.https.get(`${apiUrl}/get_main_questions`); }

  get_sub_questions(id) { return this.https.get(`${apiUrl}/get_sub_questions/${id}`); }

  submit_answers(data) {
    const httpHeader = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    const headers = {
      headers: httpHeader
    };
    const url = `${apiUrl}/submit_answers`; //route of laravel  
    return this.https.post(url, data, headers);
  }


}
