import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Configs } from '../../config';

const config = new Configs;
const apiUrl = config.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(public https: HttpClient) { }

  save_broadcast(data) {
    const httpHeader = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    const headers = {
      headers: httpHeader
    };
    const url = `${apiUrl}/save_broadcast`; //route of laravel  
    return this.https.post(url, data, headers);
  }

  get_user_broadcasts(id) { return this.https.get(`${apiUrl}/get_user_broadcasts/${id}`); }

}
