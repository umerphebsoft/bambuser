import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Configs } from '../../config';

const config = new Configs;
const apiUrl = config.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class FriendService {

  constructor(public https: HttpClient) { }

  get_friends_list(id) { return this.https.get(`${apiUrl}/get_friends_list/${id}`); }

  get_related_people(id) { return this.https.get(`${apiUrl}/get_related_people/${id}`); }


  add_friend(data) {
    const httpHeader = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    const headers = {
      headers: httpHeader
    };
    const url = `${apiUrl}/add_friend`; //route of laravel  
    return this.https.post(url, data, headers);
  }






}
