import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Configs } from '../../config';

const config = new Configs;
const apiUrl = config.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(public https: HttpClient) { }

  register_user(data) {
    const httpHeader = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    const headers = {
      headers: httpHeader
    };
    const url = `${apiUrl}/add_user`; //route of laravel  
    return this.https.post(url, data, headers);
  }

  get_role_id() { return this.https.get(`${apiUrl}/get_role_id`); }
}
