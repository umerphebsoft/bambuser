import { Injectable } from '@angular/core';
import { CanLoad } from '@angular/router';
import { AuthenticationService } from './Authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanLoad {

  constructor(
    public authenticationService: AuthenticationService,
  ) { }

  canLoad(): boolean {
    return this.authenticationService.isAuthenticated();
  }

}
