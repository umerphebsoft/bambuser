import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Configs } from '../../config';
/*
  Generated class for the SettingProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

const config = new Configs;
const apiUrl = config.apiUrl;


@Injectable({
  providedIn: 'root'
})
export class SettingService {

  constructor(public https: HttpClient) { }

  get_readonly_API_key() { return this.https.get(`${apiUrl}/get_readonly_API_key`); }

  get_app_id() { return this.https.get(`${apiUrl}/get_app_id`); }
}
