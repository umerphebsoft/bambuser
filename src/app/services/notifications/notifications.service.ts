import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Configs } from '../../config';

const config = new Configs;
const apiUrl = config.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  constructor(public https: HttpClient) { }

  send_friend_notification(data) {
    const httpHeader = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    const headers = {
      headers: httpHeader
    };
    const url = `${apiUrl}/send_friend_notification`; //route of laravel  
    return this.https.post(url, data, headers);
  }

  get_notifications_unread(id) { return this.https.get(`${apiUrl}/get_notifications_unread/${id}`); }

  get_notifications(id) { return this.https.get(`${apiUrl}/get_notifications/${id}`); }

  mark_read_notify(id) { return this.https.get(`${apiUrl}/mark_read_notify/${id}`); }

  delete_notification(id) { return this.https.get(`${apiUrl}/delete_notification/${id}`); }

}
