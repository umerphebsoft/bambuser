import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RecentBroadcastsPage } from './recent-broadcasts.page';

describe('RecentBroadcastsPage', () => {
  let component: RecentBroadcastsPage;
  let fixture: ComponentFixture<RecentBroadcastsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecentBroadcastsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RecentBroadcastsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
