import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RecentBroadcastsPageRoutingModule } from './recent-broadcasts-routing.module';

import { RecentBroadcastsPage } from './recent-broadcasts.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RecentBroadcastsPageRoutingModule
  ],
  declarations: [RecentBroadcastsPage]
})
export class RecentBroadcastsPageModule {}
