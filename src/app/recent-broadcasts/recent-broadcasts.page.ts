import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';

import * as moment from 'moment';
import * as agent from 'superagent';

// import { JavaScriptPlayerPage } from '../player-js/player-js';
import { Router } from '@angular/router';
import { SettingService } from '../services/setting/setting.service';
import { UserService } from '../services/user/user.service';

// API key generated at https://dashboard.bambuser.com/developer
import { Storage } from '@ionic/storage';
import { NotifyComponent } from '../components/notify/notify.component';

@Component({
  selector: 'app-recent-broadcasts',
  templateUrl: './recent-broadcasts.page.html',
  styleUrls: ['./recent-broadcasts.page.scss'],
})
export class RecentBroadcastsPage implements OnInit {


  READONLY_API_KEY: any = "";
  response: any;
  user_broadcasts: any;
  user_id: any = '';
  //agent = '*';
  isFetching = false;
  errorMessage = '';
  mayShowSpinner = true;
  refresher: any;
  broadcasts = [];
  moment: any;

  constructor(
    public notify: NotifyComponent,
    public storage: Storage,
    public user_API: UserService,
    public api: SettingService,
    public router: Router,
    public modalCtrl: ModalController,
    public navCtrl: NavController) {
  }//;end of constructor

  logout() {
    this.storage.clear();
    // alert('logout');
    this.router.navigate(['/login'], { skipLocationChange: true });
  }

  exist(broadcast_id) {
    var matched = false;
    this.user_broadcasts.forEach(user_broadcast => {
      if (user_broadcast.broadcast_id == broadcast_id) {
        matched = true;
      }
    });
    return matched;
  }

  ionViewDidEnter() {
    this.storage.get('user_id').then((response) => {
      if (response) {
        this.user_id = response;
      }
    });

    this.moment = moment; // provide to template
    this.GetReadOnlyApiKey();

  }
  ngOnInit() {
    // ...
  }


  GetUserBroadcasts() {

    //alert('GetUserBroadcasts');
    this.user_API.get_user_broadcasts(this.user_id).subscribe(response => {
      if (response) {
        this.user_broadcasts = response;
      }
      else {
        this.user_broadcasts = [];
      }
      // this.reloadData();
    }, error => {
      console.log("Error: ");
      console.log(error);
    });

  }

  GetReadOnlyApiKey() {
    console.log('read only api');
    this.api.get_readonly_API_key().subscribe(response => {
      if (response) {

        this.response = response;
        this.READONLY_API_KEY = this.response.app_id;
        //alert('readonly api key' + this.READONLY_API_KEY);
      }
      else {
        this.READONLY_API_KEY = '';
      }
      this.GetUserBroadcasts();
      this.reloadData();
      // alert('read only api key ' + this.READONLY_API_KEY);
      console.log('read only api key ', this.READONLY_API_KEY);
      console.log('showing broadcast list');
    }, error => {
      // alert('Error getting read only api key ' + error.message);
      console.log("Error: ");
      console.log(error);
    });
  }

  reloadData() {
    //alert('relaod data');
    if (this.READONLY_API_KEY.endsWith('NGEME')) {
      new Promise(resolve => setTimeout(resolve, 500)).then(() => {
        // Let page animations to finish before using alert()
        alert('Warning: READONLY_API_KEY is not set. Get your key at https://dashboard.bambuser.com/developer and update pages/broadcast-list/broadcast-list.ts, then rebuild the app.');
      });
      return;
    }
    // TODO: support pagination / endless scroll
    this.errorMessage = '';
    this.isFetching = true;
    console.log('Fetching broadcast list');
    // https://bambuser.com/docs/api/get-broadcast-metadata/#get-metadata-for-multiple-broadcasts
    return agent.get('https://api.bambuser.com/broadcasts')
      .set('Accept', 'application/vnd.bambuser.v1+json')
      .set('Authorization', 'Bearer ' + this.READONLY_API_KEY)
      .then(res => {
        console.log('got broadcast response', res.body);
        this.isFetching = false;
        console.log('asdasd', res.body.results.length);
        this.broadcasts = res.body.results;

      }).catch(e => {
        console.log('error while fetching broadcasts', e);
        this.errorMessage = e.message;
        this.isFetching = false;
      });
  }//end of reload data

  GetName(broadcast_id) {
    var user = 'user';
    this.user_broadcasts.forEach(user_broadcast => {
      if (user_broadcast.broadcast_id == broadcast_id) {
        user = user_broadcast.name;
        return user;
      }
    });

    return user;
  }

  timeConverter(UNIX_timestamp) {
    var a = new Date(UNIX_timestamp * 1000);
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
    return time;
  }

  onPullToRefresh(refresher) {
    // don't show our own spinner: refresher component has an internal spinner
    this.mayShowSpinner = false;
    this.reloadData().then(() => {
      refresher.complete();
      this.mayShowSpinner = true;
    })
  }

  playBroadcast(broadcast) {
    //alert('play broadcast called');
    // In this demo we choose to start live broadcasts in a native player SDK,
    // since it supports low latency playback in more scenarios.
    // and we choose to use the JavaScript player API for archived videos,
    // since it has a richer UI and event model.
    //
    // Both players are capable of both live and archived playback though:
    // for your own app: try both and make your own decision on which player to use when.
    if (broadcast.type === 'live' && window['bambuser'] && window['bambuser']['player']) {
      // Push native player page onto the navigation stack
      // it does not render correctly in a modal
      // 
      // When using a modal, the elements of the parent page remains
      // behind the modal, which defeats or strategy of using a transparent
      // webview to reveal the native player underneath.

      // this.router.navigate(['/tab1'], []);
      //alert('Native player if called : ' + broadcast.resourceUri);


      this.router.navigate(['native-player', { resourceUri: broadcast.resourceUri, autoplay: true }]);
    } else {
      // Open js player page in a modal window
      // and instruct it to play the selected broadcast
      // const playerModal = this.modalCtrl.create(Tab1Page, {
      //   resourceUri: broadcast.resourceUri,
      //   autoplay: true,
      //   showCloseButton: true,
      // });
      // playerModal.present();

      //alert('Native player else called : ' + broadcast.resourceUri);
      this.router.navigate(['native-player', { resourceUri: broadcast.resourceUri, autoplay: true }]);
      // this.router.navigate(Tab1Page, {
      //   resourceUri: broadcast.resourceUri,
      //   autoplay: true,
      // });
    }
  }

  ionViewWillLeave() {
    console.log('leaving broadcast list');
  }

}
