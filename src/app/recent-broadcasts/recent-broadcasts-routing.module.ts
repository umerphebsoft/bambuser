import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RecentBroadcastsPage } from './recent-broadcasts.page';

const routes: Routes = [
  {
    path: '',
    component: RecentBroadcastsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RecentBroadcastsPageRoutingModule {}
