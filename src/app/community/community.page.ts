import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { FriendService } from '../services/friends/friend.service';

@Component({
  selector: 'app-community',
  templateUrl: './community.page.html',
  styleUrls: ['./community.page.scss'],
})
export class CommunityPage implements OnInit {

  response: any;
  friends_list: any;
  user_id: any;
  broadcaster: any;

  constructor(
    public storage: Storage,
    public frndAPI: FriendService,
  ) {

  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.storage.get('user_id').then((response) => {
      if (response) {
        this.user_id = response;
        this.GetFriends();
      }
    });

  }

  GetFriends() {
    console.log('user id is ', this.user_id)
    this.frndAPI.get_friends_list(this.user_id).subscribe(response => {
      this.response = response;
      if (this.response.code == 1) {
        this.friends_list = this.response.friends;

      }
      else if (this.response.code == 0) {
        alert('Cannot get friends list.');
      }

    }, error => {
      alert('Error: ' + error);
    });
  }

}
