import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService as AuthGuard } from './services/auth-guard.service';

const routes: Routes = [

  {
    path: 'native-player',
    loadChildren: () => import('./native-player/native-player.module').then(m => m.NativePlayerPageModule)
  },
  {
    path: 'signup',
    loadChildren: () => import('./signup/signup.module').then(m => m.SignupPageModule),
    //canLoad: [AuthGuard]
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule),
    //canLoad: [AuthGuard]

  },
  {
    path: 'recent-broadcasts',
    loadChildren: () => import('./recent-broadcasts/recent-broadcasts.module').then(m => m.RecentBroadcastsPageModule),
    // canLoad: [AuthGuard]
  },
  {
    path: 'broadcaster',
    loadChildren: () => import('./broadcaster/broadcaster.module').then(m => m.BroadcasterPageModule),
    // canLoad: [AuthGuard]
  },
  {
    path: 'questionnaire',
    loadChildren: () => import('./questionnaire/questionnaire.module').then(m => m.QuestionnairePageModule),
    //canLoad: [AuthGuard]
  },

  {
    path: 'thanks',
    loadChildren: () => import('./thanks/thanks.module').then(m => m.ThanksPageModule)
  },
  {
    path: 'community',
    loadChildren: () => import('./community/community.module').then( m => m.CommunityPageModule)
  },
  {
    path: 'notifications',
    loadChildren: () => import('./notifications/notifications.module').then( m => m.NotificationsPageModule)
  },
  {
    path: 'add-friends',
    loadChildren: () => import('./add-friends/add-friends.module').then( m => m.AddFriendsPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
