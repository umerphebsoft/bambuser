import { Component, OnInit } from '@angular/core';
import { Platform, ToastController } from '@ionic/angular';
import { SettingService } from '../services/setting/setting.service';
import { UserService } from '../services/user/user.service';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-broadcaster',
  templateUrl: './broadcaster.page.html',
  styleUrls: ['./broadcaster.page.scss'],
})
export class BroadcasterPage implements OnInit {

  APPLICATION_ID: string = '';
  response: any;
  class: string = '';

  user_id: any = '';

  isBroadcasting = false;
  isPending = false;
  broadcaster: any;
  errListenerId = false;
  broadcastListenerId = false;

  constructor(
    public router: Router,
    public user_api: UserService,
    public api: SettingService,
    public storage: Storage,
    private toastCtrl: ToastController,
    public platform: Platform) {
  }//end of constructor


  ionViewWillEnter() {
    this.storage.get('user_id').then((response) => {
      if (response) {
        this.user_id = response;
        this.GetApplicationID();
      }
    });
  }

  ngOnInit() { }

  GetApplicationID() {
    console.log('read only api');
    this.api.get_app_id().subscribe(response => {
      if (response) {

        this.response = response;
        this.APPLICATION_ID = this.response.app_id;
        this.MyionViewDidEnter();
      }
      else {
        this.APPLICATION_ID = '';
      }
      //alert('application id ' + this.APPLICATION_ID);
      console.log('application id ', this.APPLICATION_ID);
    }, error => {
      console.log("Error: ");
      console.log(error);
    });
  }



  async MyionViewDidEnter() {
    console.log('User ID : ', this.user_id);

    this.platform.ready().then(() => {
      //alert('platform ready');
      // Using array syntax workaround, since types are not declared.
      if (window['bambuser']) {
        this.broadcaster = window['bambuser']['broadcaster'];
        console.log('broadcaster', this.broadcaster);
        // alert('broadcaster window' + window['bambuser']['broadcaster']);
        this.broadcaster.setApplicationId(this.APPLICATION_ID);
        //alert('broadcaster set app id ' + this.broadcaster);

      } else {
        // Cordova plugin not installed or running in a web browser
        alert(' Cordova plugin not installed or running in a web browser');
      }
    });


    if (this.APPLICATION_ID.endsWith('NGEME')) {
      await new Promise(resolve => setTimeout(resolve, 500)); // Let page animations to finish before using alert()
      alert('Warning: APPLICATION_ID is not set. Get your application id at https://dashboard.bambuser.com/developer and update pages/broadcaster/broadcaster.ts, then rebuild the app.');
    }

    // Engage our Ionic CSS background overrides that ensure viewfinder is visible.
    this.class = 'myclass';
    document.getElementsByTagName('body')[0].classList.add("show-viewfinder");

    if (!this.platform.is('cordova')) {
      await new Promise(resolve => setTimeout(resolve, 500)); // Let page animations to finish before using alert()
      alert('This Ionic app is currently not running within a Cordova project. Broadcasting is only supported on iOS and Android devices.');
      return;
    }

    await this.platform.ready();

    if (!this.broadcaster) {
      await new Promise(resolve => setTimeout(resolve, 5000)); // Let page animations to finish before using alert()
      alert('Broadcasting plugin not detected. Try running `cordova plugin add cordova-plugin-bambuser` and rebuild your app.');
      return;
    }

    console.log('Starting viewfinder');
    //alert('Starting viewfinder');
    this.broadcaster.showViewfinderBehindWebView();
  }

  ionViewWillLeave() {
    this.class = 'no_myclass';
    document.getElementsByTagName('body')[0].classList.remove("show-viewfinder");
    this.broadcaster.hideViewfinder();
  }

  async start() {
    if (this.isBroadcasting || this.isPending) return;
    this.isPending = true;
    const toast = this.toastCtrl.create({
      message: 'Starting broadcast...',
      position: 'middle',
    });
    (await toast).present();

    console.log('Starting broadcast');
    try {
      await this.broadcaster.startBroadcast();
      (await toast).dismiss();
      this.isBroadcasting = true;
      this.isPending = false;

      this.listenForError();
      this.listenForBroadcastId();

    } catch (e) {
      (await toast).dismiss();
      this.isPending = false;
      alert('Failed to start broadcast');
      console.log(e);
    }
  }

  async stop() {
    if (!this.isBroadcasting || this.isPending) return;
    this.isPending = true;
    const toast = this.toastCtrl.create({
      message: 'Ending broadcast...',
      position: 'middle'
    });
    (await toast).present();

    console.log('Ending broadcast');
    try {
      await this.broadcaster.stopBroadcast();
      (await toast).dismiss();
      this.isBroadcasting = false;
      this.isPending = false;
      //when the broadcast is stopped we need to ask question about broadcast so we are navigating user to question page
      this.router.navigate(['/questionnaire']);

    } catch (e) {
      (await toast).dismiss();
      this.isPending = false;
      alert('Failed to stop broadcast');
      console.log(e);
    }
  }

  listenForError() {
    if (this.errListenerId) return;
    this.errListenerId = this.broadcaster.addEventListener('connectionError', status => {
      this.isBroadcasting = false;
      this.isPending = false;
      const toast = this.toastCtrl.create({
        message: 'Connection error',
        position: 'middle',
        duration: 3000,
      });
      alert('Connecton Error: ' + status);
      // toast.present();            
    });
  }

  listenForBroadcastId() {
    if (this.broadcastListenerId) return;
    this.broadcastListenerId = this.broadcaster.addEventListener('broadcastIdAvailable', broadcastId => {
      const toast = this.toastCtrl.create({
        message: `Broadcast id received: ${broadcastId}`,
        position: 'middle',
        duration: 3000,
      });
      this.save_broadcast(broadcastId);
      //saving broadcast in session for question answers      
      this.storage.set('broadcast_id', broadcastId);

      //broadcastId is the id and is called when a broadcast is made...
      //toast.present();
      // alert('broadcast id is : ' + broadcastId);
    });
  }

  switchCamera() {
    if (this.broadcaster) {
      this.broadcaster.switchCamera();
    }
  }

  async save_broadcast(broadcast_id) {

    var formdata = {
      user_id: this.user_id,
      broadcast_id: broadcast_id,
    };

    // alert('form data ' + formdata.broadcast_id);

    await this.user_api
      .save_broadcast(formdata) //api service method
      .subscribe((response) => {
        this.response = response;
        if (this.response.code != 1) {
          alert('something went wrong. Your this broadcast will not be saved');
        }
      },
        (err) => {
          alert('Something went wrong. Your this broadcast will not be saved. ' + err.message);
        }
      );
  }//end of save_broadcast


}
