import { Component, OnInit } from '@angular/core';
import { FriendService } from '../services/friends/friend.service';
import { Storage } from '@ionic/storage';
import { NotificationsService } from '../services/notifications/notifications.service';
@Component({
  selector: 'app-add-friends',
  templateUrl: './add-friends.page.html',
  styleUrls: ['./add-friends.page.scss'],
})
export class AddFriendsPage implements OnInit {

  items = [];
  mydone = true;
  user_id: any;
  response: any;
  people: any;
  constructor(
    public notificationAPI: NotificationsService,
    public friendAPI: FriendService,
    public storage: Storage
  ) {
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.storage.get('user_id').then((response) => {
      if (response) {
        this.user_id = response;
        this.GetRelatedPeople();
      }
    });
  }

  Send_Request(id) {
    var ele = 'btn_' + id;
    var btn = document.getElementById(ele) as HTMLButtonElement;
    btn.disabled = true;
    btn.innerText = 'Sent';
    //this is used to send notification of frnd request to db and then to user
    var formdata = {
      "from_id": this.user_id,
      "to_id": id
    }
    console.log('form data', formdata);
    this.notificationAPI
      .send_friend_notification(formdata) //api service method
      .subscribe((response) => {
        this.response = response;
        console.log(this.response);
        if (this.response.code == 0) {
          //this.presentToast(this.response.message);
          alert('Error Cannot Send Request');
        }
      },
        (err) => {
          alert('Error: ' + err.message);
        }
      );
    //this is used to send notification of frnd request to db and then to user ends here
  }//end of Send_Request

  loadData(event) {
    if (this.items.length != this.people.length) {
      var length = this.items.length;
      console.log('length is ', length);
      if ((this.people.length - length) >= 5) {
        for (var i = length; i <= (length + 5); i++) {
          this.items.push(this.people[i]);
        }//end of for
      }
      else {
        for (var i = length; i <= this.people.length; i++) {
          this.items.push(this.people[i]);
        }//end of for
      }
    }
    else {
      console.log('my done false');
      this.mydone = false;
    }

  }//end of loadData

  GetRelatedPeople() {
    var counter = 0;
    this.friendAPI.get_related_people(this.user_id).subscribe(response => {
      this.response = response;
      console.log('response ', this.response);
      if (this.response.code == 1) {
        this.people = this.response.friends;
        console.log('this is response ', this.response);
        this.people.forEach(element => {
          this.items.push(element);
          counter++;
          if (counter == 5) {
            return false;
          }
        });//end of foreach loop to put 10 items in items                
      }
      else if (this.response.code == 0) {
        alert('Cannot get related people.');
      }

    }, error => {
      alert('Error: ' + error);
    });
  }//end of GetRelatedPeople

}
